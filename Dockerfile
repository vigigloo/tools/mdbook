ARG BUILDTIME_FROM=rust:1.71
ARG RUNTIME_FROM=gcr.io/distroless/cc


FROM $BUILDTIME_FROM as build-env
ARG MDBOOK_VERSION="0.4.34"
ARG MDBOOK_LINKCHECK_VERSION="0.7.7"
ARG MDBOOK_MERMAID_VERSION="0.12.6"
ARG MDBOOK_TOC_VERSION="0.14.1"
ARG MDBOOK_PLANTUML_VERSION="0.8.0"
ARG MDBOOK_OPEN_ON_GH_VERSION="2.4.1"
ARG MDBOOK_GRAPHVIZ_VERSION="0.1.6"
ARG MDBOOK_KATEX_VERSION="0.5.7"
RUN apt-get update && apt-get install build-essential cmake graphviz -y

RUN cargo install --version $MDBOOK_VERSION mdbook
RUN cargo install --version $MDBOOK_LINKCHECK_VERSION mdbook-linkcheck
RUN cargo install --version $MDBOOK_MERMAID_VERSION mdbook-mermaid
RUN cargo install --version $MDBOOK_TOC_VERSION mdbook-toc
RUN cargo install --version $MDBOOK_PLANTUML_VERSION mdbook-plantuml
RUN cargo install --version $MDBOOK_OPEN_ON_GH_VERSION mdbook-open-on-gh
RUN cargo install --version $MDBOOK_GRAPHVIZ_VERSION mdbook-graphviz
RUN cargo install --version $MDBOOK_KATEX_VERSION mdbook-katex

FROM $RUNTIME_FROM
COPY --from=build-env /usr/bin/dot /usr/bin/dot
COPY --from=build-env /usr/local/cargo/bin/mdbook* /usr/local/bin/
CMD ["/usr/local/bin/mdbook"]
